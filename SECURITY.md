<!--
This file is part of DSGOV.BR - Web Components.

DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
at your option) any later version.

DSGOV.BR - Web Components is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.

Copyright 2021 Serpro.
 -->

# Reportando problemas de segurança

> Por favor não poste problemas de segurança nos canais abertos, como issues e discord, a menos que seja estritamente necessário.

Nós do time DSGOV.BR, juntamente com a comunidade, levamos segurança muito a sério.

Solicitamos que problemas desse tipo sejam reportados pelo email <dsgov@serpro.gov.br> e que seja incluída a palavra "SEGURANÇA" no assunto para facilitar a classificação.

Nós agradecemos todo esforço para nos reportar problemas de segurança de maneira responsável e segura. Nós vamos nos esforçar para reconhecer todas as contribuições

Iremos nos esforçar ao máximo para responder tais problemas de maneira rápida, eficiente e te mantendo atualizado.
