/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import Checkbox from '../Checkbox/Checkbox.ce.vue'
import { kebabiseArgs } from '../../util/Utils.js'

export default {
  title: 'Dsgov/br-checkbox',
  component: Checkbox,
  parameters: {
    controls: { exclude: ['value', 'v-model'] },
  },
  argTypes: {
    /** **[Opcional]** Texto descritivo, localizado sempre à direita da caixa de opção.*/
    label: {
      defaultValue: '',
    },
    /** **[Opcional]** Desabilita o checkbox. */
    disabled: {
      defaultValue: false,
    },
    format: {
      control: {
        type: 'select',
        options: ['valid', 'invalid'],
      },
    },
    /** **[Opcional]** Formata o componente para versão horizontal. */
    inline: {
      defaultValue: false,
    },
    /** **[Opcional]** Acessibilidade: define uma cadeia de caracteres para descrever o elemento. */
    ariaLabel: {
      defaultValue: '',
    },
    /** **[Opcional]** Define o name que será atribuido ao checkbox. */
    name: {
      defaultValue: '',
    },
    /** Estado checked. */
    checked: {
      type: 'boolean',
      defaultValue: false,
    },
    /** **[Opcional]** Ativa a aparência indeterminada da caixa de seleção. */
    indeterminate: {
      defaultValue: false,
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-checkbox v-bind="args"></br-checkbox>`,
})

export const Base = Template.bind({})
Base.args = {
  label: 'Label do checkbox',
  name: 'base',
  model: 'check1',
}

export const WithoutLabel = Template.bind({})
WithoutLabel.args = {
  vModel: 'check2',
}

export const Checked = Template.bind({})
Checked.args = {
  label: 'Label do checkbox',
  checked: 'checked',
  vModel: 'check3',
}

export const Invalid = Template.bind({})
Invalid.args = {
  label: 'Label do checkbox',
  format: 'invalid',
  // vModel: 'check4',
}

export const Valid = Template.bind({})
Valid.args = {
  label: 'Label do checkbox',
  format: 'valid',
}

export const Disabled = Template.bind({})
Disabled.args = {
  label: 'Label do checkbox',
  disabled: true,
}

const TemplateMultiple = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div>
  <p class="label mb-0">Rótulo</p>
  <p class="text-down-01">Informações adicionais</p>
  <div class="mb-1">
    <br-checkbox label="Unchecked"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Checked" checked="checked"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Valid" format="valid"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Invalid" format="invalid"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Disabled" disabled></br-checkbox>
  </div>
</div>`,
})

export const Multiple = TemplateMultiple.bind({})
Multiple.parameters = {
  controls: {
    exclude: [
      'label',
      'disabled',
      'inline',
      'value',
      'v-model',
      'checked',
      'indeterminate',
    ],
  },
}

const TemplateInline = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div>
  <p class="label mb-0">Rótulo</p>
  <p class="text-down-01">Informações adicionais</p>
  <br-checkbox label="Unchecked" inline></br-checkbox>
  <br-checkbox label="Checked" checked="checked" inline></br-checkbox>
  <br-checkbox label="Valid" format="valid" inline></br-checkbox>
  <br-checkbox label="Invalid" format="invalid" inline></br-checkbox>
  <br-checkbox label="Disabled" disabled inline></br-checkbox>
</div>
  `,
})

export const Inline = TemplateInline.bind({})
Inline.parameters = {
  controls: {
    exclude: [
      'label',
      'disabled',
      'inline',
      'value',
      'v-model',
      'checked',
    ],
  },
}

export const Indeterminate = Template.bind({})
Indeterminate.parameters = {
  controls: { exclude: [/* 'checked',  */'value', 'v-model'] },
}
Indeterminate.args = {
  label: 'Label do checkbox',
  indeterminate: true,/* 
  checked: true, */
}
