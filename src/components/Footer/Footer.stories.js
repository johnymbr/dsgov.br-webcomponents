/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrFooter from './Footer.ce.vue'
import { kebabiseArgs } from '../../util/Utils.js'
import BrFooterLogo from './FooterLogo/FooterLogo.ce.vue'
import BrFooterImage from './FooterImage/FooterImage.ce.vue'
import BrFooterSocialNetwork from './FooterSocialNetwork/FooterSocialNetwork.ce.vue'
import BrFooterSocialNetworkItem from './FooterSocialNetworkItem/FooterSocialNetworkItem.ce.vue'

const slotSocialNetwork = `
  <br-footer-social-network label="Redes Sociais" slot="redesSociais">
    <br-footer-social-network-item url="http://www.facebook.com">
      <span class="fab fa-facebook"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="http://www.twitter.com">
      <span class="fab fa-twitter"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="http://www.instagram.com">
      <span class="fab fa-instagram"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="http://www.linkedin.com">
      <span class="fab fa-linkedin"></span>
    </br-footer-social-network-item>
  </br-footer-social-network>
      `
const slotLogo = `
  <br-footer-logo src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-negative.png"
    alt="Acesso à informação"
    slot="logo" centered="false"></br-footer-logo>`

const slotLogoVazio = `
  <br-footer-logo slot="logo"></br-footer-logo>`

const slotLogoInverted = `
  <br-footer-logo src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-negative.png"
    alt="Acesso à informação"
    slot="logo" inverted="true"></br-footer-logo>`

const slotImagensInverted = `
  <br-footer-image src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png"
    alt="Acesso à informação"
    href="#acesso-a-informacao1"
    slot="footerImagens" inverted="true"></br-footer-image>
  <br-footer-image
    src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png"
    alt="Acesso à informação"
    href="#acesso-a-informacao2"
    slot="footerImagens" inverted="true"></br-footer-image>
`

const slotImagens = `
  <br-footer-image src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png"
    alt="Acesso à informação"
    href="#acesso-a-informacao1"
    slot="footerImagens"></br-footer-image>
  <br-footer-image
    src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png"
    alt="Acesso à informação"
    href="#acesso-a-informacao2"
    slot="footerImagens"></br-footer-image>
`

export default {
  title: 'Dsgov/br-footer',
  component: BrFooter,
  subcomponents: {
    'br-footer-logo': BrFooterLogo,
    'br-footer-image': BrFooterImage,
    'br-footer-social-network': BrFooterSocialNetwork,
    'br-footer-social-network-item': BrFooterSocialNetworkItem,
  },
  argTypes: {
    redesSociais: {
      description:
        '**[OPCIONAL]** Links das redes sociais, que pode ser passado por slot.',
      control: 'text',
      type: {
        required: true,
      },
      defaultValue: '',
    },
    logo: {
      description:
        '**[OBRIGATÓRIO]** Conteúdo da logo, que deve ser passado por slot.',
      control: 'text',
      type: {
        required: true,
      },
      defaultValue: '',
    },
    footerImagens: {
      description:
        '**[OPCIONAL]** Imagens do rodapé, que pode ser passado por slot.',
      control: 'text',
      type: {
        required: true,
      },
      defaultValue: '',
    },
    inverted: {
      description:
        '**[OPCIONAL]** Inverte o tema do componente para a versão mais escura.',
      control: 'boolean',
      type: {
        required: false,
      },
      defaultValue: false,
    },
    text: {
      description: '**[OBRIGATÓRIO]** Texto informativo do rodapé.',
      control: 'text',
      type: {
        required: true,
      },
      defaultValue: '',
    },
    categories: {
      description:
        '**[OBRIGATÓRIO]** Lista de todas as categorias disponiveis no footer.',
      control: 'text',
      type: {
        required: true,
      },
      defaultValue: '',
    },
  },
}

const TemplateCompleto = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-footer v-bind="args">
  ${args.redesSociais}
  ${args.logo}
  ${args.footerImagens}
</br-footer>
  `,
})

export const Completo = TemplateCompleto.bind({})
Completo.args = {
  inverted: false,
  text: 'Todo o conteúdo deste site está publicado sob a licença Creative Commons',
  logo: slotLogo,
  footerImagens: slotImagens,
  redesSociais: slotSocialNetwork,
  categories: `[
    {
      title: 'Categoria 1',
      items: [
        {title: 'item 1', href: '/item1'},
        {title: 'item 2', href: '/item1'},
        {title: 'item 3', href: '/item1'},
        {title: 'item 4', href: '/item1'}
      ]
    },
    {
      title: 'Categoria 2',
      items: [
        {title: 'item 5', href: 'http://www.google.com'},
        {title: 'item 6', href: '/item1'},
        {title: 'item 7', href: '/item1'},
        {title: 'item 8', href: '/item1'}
      ]
    }
  ]`,
}

export const Inverted = TemplateCompleto.bind({})
Inverted.args = {
  inverted: true,
  text: 'Todo o conteúdo deste site está publicado sob a licença Creative Commons',
  logo: slotLogoInverted,
  footerImagens: slotImagensInverted,
  redesSociais: slotSocialNetwork,
  categories: `[
    {
      title: 'Categoria 1',
      items: [
        {title: 'item 1', href: '/item1'},
        {title: 'item 2', href: '/item1'},
        {title: 'item 3', href: '/item1'},
        {title: 'item 4', href: '/item1'}
      ]
    },
    {
      title: 'Categoria 2',
      items: [
        {title: 'item 5', href: 'http://www.google.com'},
        {title: 'item 6', href: '/item1'},
        {title: 'item 7', href: '/item1'},
        {title: 'item 8', href: '/item1'}
      ]
    }
  ]`,
}

const TemplateApenasLinks = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-footer v-bind="args">
  ${args.logo}
</br-footer>
`,
})

export const ApenasLinks = TemplateApenasLinks.bind({})
ApenasLinks.args = {
  logo: slotLogoVazio,
  categories: `[
    {
      title: 'Categoria 1',
      items: [
        {title: 'item 1', href: '/item1'},
        {title: 'item 2', href: '/item1'},
        {title: 'item 3', href: '/item1'},
        {title: 'item 4', href: '/item1'}
      ]
    },
    {
      title: 'Categoria 2',
      items: [
        {title: 'item 5', href: 'http://www.google.com'},
        {title: 'item 6', href: '/item1'},
        {title: 'item 7', href: '/item1'},
        {title: 'item 8', href: '/item1'}
      ]
    }
  ]`,
}
