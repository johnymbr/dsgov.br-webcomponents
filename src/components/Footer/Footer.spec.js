/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from '@vue/test-utils'
import BrFooter from './Footer.ce.vue'
import BrFooterLogo from './FooterLogo/FooterLogo.ce.vue'
import BrFooterImage from './FooterImage/FooterImage.ce.vue'
import BrFooterSocialNetwork from './FooterSocialNetwork/FooterSocialNetwork.ce.vue'
import BrFooterSocialNetworkItem from './FooterSocialNetworkItem/FooterSocialNetworkItem.ce.vue'

describe('Footer', () => {
  const propCategorias = `[
    {
      title: 'Categoria 1',
      items: [
        {title: 'item 1', href: '/item1'},
        {title: 'item 2', href: '/item1'},
        {title: 'item 3', href: '/item1'},
        {title: 'item 4', href: '/item1'}
      ]
    },
    {
      title: 'Categoria 2',
      items: [
        {title: 'item 5', href: 'http://www.google.com'},
        {title: 'item 6', href: '/item1'},
        {title: 'item 7', href: '/item1'},
        {title: 'item 8', href: '/item1'}
      ]
    }
  ]`

  const textHello = 'hello'
  const wrapperFooter = shallowMount(BrFooter, {
    propsData: {
      text: textHello,
      inverted: true,
      categories: propCategorias,
    },
  })

  test('it renders footer text', () => {
    expect(wrapperFooter.text()).toMatch(textHello)
  })

  test('it renders inverted mode', () => {
    expect(wrapperFooter.find('.inverted').exists()).toBe(true)
  })

  test('it renders categories', () => {
    expect(wrapperFooter.findAll('.br-list')).toHaveLength(3)
    expect(wrapperFooter.findAll('.br-item')).toHaveLength(10)
  })

  test('it opens and close category list when click on it', () => {
    // Trigger the window resize event.
    global.innerWidth = 200
    global.dispatchEvent(new Event('resize'))
    wrapperFooter
      .find('a')
      .trigger('click')
      .then(() => {
        expect(wrapperFooter.find('.open').exists()).toBe(true)
        wrapperFooter
          .find('a')
          .trigger('click')
          .then(() => {
            expect(wrapperFooter.find('.open').exists()).toBe(false)
          })
      })
  })
})

describe('FooterLogo', () => {
  const srcLogo = 'https://url-do-logo'

  const slotLogo = `
    <br-footer-logo
    src="${srcLogo}"
    alt="Acesso à informação"
    slot="logo"></br-footer-logo>`

  const wrapperLogo = shallowMount(BrFooter, {
    slots: {
      logo: slotLogo,
    },
    global: {
      stubs: {
        'br-footer-logo': BrFooterLogo,
      },
    },
  })

  test('it renders logo', () => {
    expect(wrapperLogo.find('.logo').exists()).toBe(true)
  })

  test('it renders logo image', () => {
    expect(wrapperLogo.find("img[src='" + srcLogo + "']").exists()).toBe(true)
  })
})

describe('FooterSocialNetwork', () => {
  const label = 'Redes Sociais'
  const slotRedesSociais = `
  <br-footer-social-network slot="redes-sociais" label="${label}">
    <br-footer-social-network-item url="#facebook">
      <span class="fab fa-facebook"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="#twitter">
      <span class="fab fa-twitter"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="#instagram">
      <span class="fab fa-instagram"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="#linkedin">
      <span class="fab fa-linkedin"></span>
    </br-footer-social-network-item>
  </br-footer-social-network>
  `
  const wrapperRedesSociais = shallowMount(BrFooter, {
    slots: { redesSociais: slotRedesSociais },
    global: {
      stubs: {
        'br-footer-social-network': BrFooterSocialNetwork,
        'br-footer-social-network-item': BrFooterSocialNetworkItem,
      },
    },
  })

  test('it renders social networks', () => {
    expect(wrapperRedesSociais.find('.social-network').exists()).toBe(true)
  })

  test('it renders social networks label', () => {
    expect(wrapperRedesSociais.text()).toMatch(label)
  })

  test('it renders social networks itens', () => {
    expect(wrapperRedesSociais.find('.fa-facebook').exists()).toBe(true)
    expect(wrapperRedesSociais.find('.fa-twitter').exists()).toBe(true)
    expect(wrapperRedesSociais.find('.fa-instagram').exists()).toBe(true)
    expect(wrapperRedesSociais.find('.fa-linkedin').exists()).toBe(true)
  })
})

describe('FooterImage', () => {
  const srcImg = 'https://url-da-imagem1'
  const srcImg2 = 'https://url-da-imagem2'

  const slotImagens = `
    <br-footer-image
      src="${srcImg}"
      alt="Acesso à informação"
      href="#acesso-a-informacao1"
      slot="footerImagens">
    </br-footer-image>
    <br-footer-image
      src="${srcImg2}"
      alt="Acesso à informação"
      href="#acesso-a-informacao2"
      slot="footerImagens">
    </br-footer-image>`

  test('it renders footer images', () => {
    const wrapper = shallowMount(BrFooter, {
      slots: {
        footerImagens: slotImagens,
      },
      global: {
        stubs: {
          'br-footer-image': BrFooterImage,
        },
      },
    })
    expect(wrapper.find("img[src='" + srcImg + "']").exists()).toBe(true)
    expect(wrapper.find("img[src='" + srcImg2 + "']").exists()).toBe(true)
  })
})
