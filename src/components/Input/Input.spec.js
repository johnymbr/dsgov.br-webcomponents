/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { mount, shallowMount } from '@vue/test-utils'
import BrInput from './Input.ce.vue'

const densidades = ['large', 'medium', 'small']
const cores = ['info', 'warning', 'danger', 'success']

describe('Input', () => {
  test('find Input component and class "br-input"', () => {
    const wrapper = shallowMount(BrInput)
    expect(wrapper.classes('br-input')).toBe(true)
  })

  test('setValue on input', async () => {
    const wrapper = shallowMount(BrInput)
    const textInput = wrapper.find('input[type="text"]')
    await textInput.setValue('Inserindo texto no input')
    expect(wrapper.find('input[type="text"]').element.value).toBe(
      'Inserindo texto no input'
    )
  })

  test('set label attribute', () => {
    const rotulo = 'Label do input'
    const wrapper = shallowMount(BrInput, {
      props: {
        label: rotulo,
      },
    })
    expect(wrapper.text()).toMatch(rotulo)
  })

  test('set label-inline attribute', () => {
    const rotulo = 'Label do input'
    const wrapper = shallowMount(BrInput, {
      props: {
        'label-inline': rotulo,
      },
    })
    expect(wrapper.text()).toMatch(rotulo)
    expect(wrapper.find('.input-inline').exists()).toBe(true)
  })

  test('set icon', () => {
    const icone = 'car'
    const wrapper = shallowMount(BrInput, {
      props: {
        icon: icone,
      },
    })
    expect(wrapper.find('.input-button').exists()).toBe(true)
  })

  test('set is-highlight', () => {
    const wrapper = shallowMount(BrInput, {
      props: {
        isHighlight: true,
      },
    })
    expect(wrapper.find('.input-highlight').exists()).toBe(true)
  })

  test('set iconSign', () => {
    const iconSign = 'user'
    const wrapper = shallowMount(BrInput, {
      props: {
        iconSign: iconSign,
      },
    })
    expect(wrapper.find('.input-icon').exists()).toBe(true)
  })

  test('has the expected html structure', () => {
    const wrapper = shallowMount(BrInput)
    expect(wrapper.element).toMatchSnapshot()
  })

  densidades.forEach((densidade) => {
    test(`set density attribute ${densidade}`, () => {
      const wrapper = shallowMount(BrInput, {
        props: {
          density: densidade,
        },
      })
      expect(wrapper.find(`.${densidade}`).exists()).toBe(true)
      // expect(wrapper.element).toMatchSnapshot()
    })
  })

  cores.forEach((cor) => {
    test(`set state attribute ${cor}`, () => {
      const wrapper = shallowMount(BrInput, {
        props: {
          state: cor,
        },
      })
      expect(wrapper.find(`.${cor}`).exists()).toBe(true)
      // expect(wrapper.element).toMatchSnapshot()
    })
  })

  test('changing the element\'s value, updates the v-model', () => {
    var parent = mount({
      data () { return { login: '' } },
      template: '<div><br-input type="text" id="login" label="Login" v-model="login"></br-input></div>',
      components: { BrInput }
    })

    const username = 'username_login'
    parent.find('input').setValue(username)

    expect(parent.vm.$data.login).toBe(username);
  })
})
