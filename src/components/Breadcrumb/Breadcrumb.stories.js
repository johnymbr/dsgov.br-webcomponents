/*
 * This file is part of DSGOV.BR - Web Components.
 *
 * DSGOV.BR - Web Components is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Web Components is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Web Components. If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { kebabiseArgs } from '../../util/Utils.js'
import BrBreadcrumb from './Breadcrumb.ce.vue'
import BrCrumb from './Crumb.ce.vue'

const defaultSlotContent = `
  <br-crumb label="Início" home></br-crumb>
  <br-crumb label="Página Ancestral 01"></br-crumb>
  <br-crumb label="Página Ancestral 02" target="_blank"
      href="javascript:void(0)"></br-crumb>
  <br-crumb label="Página atual" active></br-crumb>
`

export default {
  title: 'DSGOV/br-breadcrumb',
  component: BrBreadcrumb,
  subcomponents: {
    'br-crumb': BrCrumb,
  },
  argTypes: {
    // o nome do slot default é "default" e pode ser referenciado como chave dos argTypes
    label: {
      control: 'text',
    },
    default: {
      description:
        '**[OBRIGATÓRIO]** Conteúdo da notificação, que deve ser passado por slot.',
      defaultValue: defaultSlotContent,
      control: 'text',
      type: {
        required: true,
      },
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-breadcrumb v-bind="args">${args.default}</br-breadcrumb>`,
})

export const Default = Template.bind({})
Default.args = {
  label: 'Breadcrumb Padrão',
  default: defaultSlotContent,
}
